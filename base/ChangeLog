1.2.7

2005-04-13  Murray Cumming <murrayc@murrayc.com>

	* configure.in: Revert library .so version 
	to 5.x.x. The major version should never have 
	been increased. Luckily 1.2.6 has only been online
	for a few hours.

1.2.6:

2005-01-31  Murray Cumming  <murrayc@mrrayc.com>

	* eVC4: Added Makefile.am to distribute the files.
	* configure.in: Mentioned the new Makefile.

2004-11-25  Ulrich Eckhardt <eckhardt@satorlaser.com>

 	* added projectfiles and README for building with Embedded Visual C++
 	* modified unittests to work around bug in compiler of eVC4 and VC6 
 	that causes template ctors to not be generated.
 	* added WinMain definition in eVC4/main.cpp required for eVC4

2004-08-12  Andrew Bettison  <andrewb@acres.com.au>
 
 	* sigc++/signal.cc (SignalNode::cleanup): Rewrote Martin Schulze's
 	patch (id=21942) to do proper housekeeping on bidirectional linked
 	list.
 	* sigc++/macros/signal.h.m4 (SignalNode::_remove): New private method
 	to support above.
 	* sigc++/signal.cc (SignalNode::push_front): Fixed bug #127928 by
 	changing one offending line.

2005-01-11 Abel Cheung  <deaddog@deadog.org>

	* sigc++-1.2.pc.in: Correct include path for 64-bit systems.

2003-08-06  Martin Schulze  <teebaum@cvs.gnome.org>

	* sigc++/object.{cc,h}: Revert last Change. Murray doesn't
	allow API addition.

2003-08-06  Martin Schulze  <teebaum@cvs.gnome.org>

	* sigc++/object.{cc,h}: Add ObjectBase::clear() which destroys
	the member control_ notifying its dependencies.
	(Bug #118745 - requested by Eric NICOLAS)

2003-07-09  Philipp Thomas  <pthomas@suse.de>

	* libsigc++.spec.in: Use %{_libdir} instead of %{prefix}/lib.
	Pass %{_libdir} to configure.
	Use DESTDIR for 'make install'.
	Consistently put variable names in brackets.
	Include the directory %{_libdir}/%{lib_name} in devel package.
	
2003-06-15  Andreas Rottmann  <rottmann@users.sourceforge.net>

	* configure.in (LIBSIGC_SO_VERSION): Bumped to 6:0:1 (because of
	ABI addition, see below).

	* sigc++/object.h (Ptr): Removed silly test assignments.
	(ObjectBase): Added const control() method (fixes Ptr<const Foo>).

2003-05-20  Murray Cumming  <murrayc@usa.net>

	* Makefile.am: Don't distribute the old MSVC directory.Distribute the
	new MSVC_6 and MSVC_Net2003 ones.
	
1.2.5:

2003-05-15  Murray Cumming  <murrayc@usa.net>

	* Added John Burris's MSVC++ .NET 2003 project files. Moved the MSVC6
	stuff into the MSVC_6 folder.

2003-05-10  John Burris <burrisj@linkline.com>

        * sigc++/config/sigcconfig.h.in: Added support for Visual Studio .NET 2003. 

2003-05-08  Cedric Gustin <cedric.gustin@swing.be>

        * sigc++/object.h: Added some missing LIBSIGC_API for win32. 

2003-05-07  Murray Cumming  <murrayc@usa.net>

	* sigc++/object.h: (ObjectBase::operator=) commented-out unused 
	parameter name.

2003-03-26  Ulrich Eckhardt <eckhardt@satorlaser.com>
	
	* added missing LIBSIGC_API macro to class ConstMethodSlotNode and class
	MethodSlotNode, for MSVC++ import/export.

1.2.4:

2003-03-14  Ulrich Eckhardt <eckhardt@satorlaser.com>

	* now really apply last part of previous changelog
	* renamed the created DLLs from libsigc++-1.2.dll to libsigc++1.2-vc6.dll
	to avoid ABI conflicts with DLLs built by other compilers

2003-02-10  Ulrich Eckhardt

	* reordered cctor and assignment-op of class Ptr<> so the templated 
	version comes before the non-template version because VC6 chokes on
	it if not.
	* only supply partial specializations of Signal#<> and 
	AdaptorRetypeSlot for void-returns if the compiler supports 
	it (SIGC_CXX_PARTIAL_SPEC).
	* added a missing 'return 0;' to main() in signal_test and bulk_test
	* MSVC6 fails to deduce which rebind<> template to choose, added 
	a version that explicit names the number of arguments. Modified 
	retype_test.cc accordingly.
	* modified some reinterpret_cast<> from one type of pointer-to-
	memberfunction to another to use references instead.
	* replaced the 'void (Object::*Method)()' with a typedef that does not
	involve any real class to keep the compiler (in particular the one that
	comes with VC6) from doing any conversions between related types.

2003-01-17  Ulrich Eckhardt

	* Added LIBSIGC_API macro to classes, for MSVC++ import/export.

1.2.3:

2003-01-06  Murray Cumming  <murrayc@usa.net>

	* removed top-level sigc++.h, which was not used. There's a real one in
	sigc++/sigc++.h

2002-12-14  Daniel Elstner  <daniel.elstner@gmx.net>

	* tests/disappearing_observer_test.cc (main): Avoid warning about
	unused parameters.

2002-12-11  Tim Shead  <tshead@k-3d.com>

	* sigc++/signal.cc: Fix segfault when a signal observer deletes
	itself from within its signal handler.
	* tests/Makefile.am: Added regression test for this scenario.
	* tests/disappearing_observer.cc: New regression test source.

2002-12-06  Murray Cumming  <murrayc@usa.net>

	* Added doc/website, not distributed. This is Morten Brix Pedersen's
	SSI/css-ization of the website, making it easier to maintain.

2002-12-04  Daniel Elstner  <daniel.elstner@gmx.net>

	* Makefile.am (ACLOCAL_AMFLAGS): Fix usage of this variable, so
	that automagic rebuilding of configure now works.

1.2.2:

2002-11-30  Daniel Elstner  <daniel.elstner@gmx.net>

	* sigc++/macros/class_slot.h.m4: Fix cut'n'paste comments.

2002-11-08  Daniel Elstner  <daniel.elstner@gmx.net>

	* sigc++/macros/object_slot.h.m4: Make sure the type of the control
	object (O1) and the PMF (pointer to member function) class type (O2)
	are compatible.  This is achived by forcing an implicit cast from
	O1& to O2& via an assignment.

2002-11-06  Cedric Gustin  <cgustin@ibelgique.com>

	* configure.in : Added LIBSTDCPP_LIBS variable on win32 platforms.
	This is required when building DLLs on either mingw32 or cygwin.
	* sigc++/Makefile : Added LIBSTDCPP to libstdc_1_2_la_LDFLAGS. Added
	local install rule for the .dll.a import library file on win32. 

2002-11-05  John Burris  <burrisj@linkline.com>

	* sigc++/config/sigcconfig.in: Added support for the INTEL
	compiler on win32.
	* README.intel.win32: Added documentation for INTEL compiler.
	* win32_intel/: Added Visual C++ 6.0 project files.

2002-10-31  Daniel Elstner  <daniel.elstner@gmx.net>

	* sigc++/object.cc (Control_::destroy): Get the next node *before*
	notifying the current one.  This prevents access to freed memory,
	as reported by Andreas Rottmann <a.rottmann@gmx.at>.
	* sigc++/slot.cc (SlotNode::notify): ditto.

1.2.1:
	
2002-10-18  Daniel Elstner  <daniel.elstner@gmx.net>

	* sigc++/node.h (NodeBase::notify): Add TODO comment asking for
	clear documentation of the from_child parameter.

2002-10-18  Daniel Elstner  <daniel.elstner@gmx.net>

	* sigc++/connection.cc (ConnectionNode::notify): Don't invoke
	slot()->remove_dependency(this) if the node has already been
	notified.  Fixes another crash on disconnect().

2002-10-18  Daniel Elstner  <daniel.elstner@gmx.net>

	* configure.in: Bump version to 1.2.1 so I can make regexxer
	require it ;)

2002-10-17  Daniel Elstner  <daniel.elstner@gmx.net>

	* sigc++/signal.cc (SignalNode::cleanup): Reset the parent_ pointer
	of all unreferenced nodes.  The code is still a bit unclear to me,
	therefore I added a comment describing a potential problem.

2002-10-17  Daniel Elstner  <daniel.elstner@gmx.net>

	* sigc++/signal.cc (SignalNode::clear): When removing a node,
	clear the parent_ pointer of the node to indicate the removal.
	(SignalNode::cleanup): ditto
	(SignalNode::remove): ditto.
	(SignalConnectionNode::notify): Make sure the node still has
	a parent before invoking parent_->remove(this).  This fixes a
	crash when calling disconnect() on a connection whose parent
	already died.

2002-10-11  Daniel Elstner  <daniel.elstner@gmx.net>

	Warning cleanups for g++-3.2 -pedantic -W -Wall:

	* sigc++/node.cc (NodeBase::notify): Avoid compiler warning
	about an unused parameter.
	* tests/bind_test.cc (foo2): ditto
	* sigc++/macros/template.macros.m4: Introduce new m4 macro
	ARG_REFTYPE which does the same as ARG_REF but omits parameter
	names.
	* sigc++/macros/hide.h.m4: Use ARG_REFTYPE to avoid warnings
	about unused parameters (i.e. the ones to hide).

2002-10-11  Daniel Elstner  <daniel.elstner@gmx.net>

	* configure.in: Clean up the code checking whether the linker
	supports global ctors, in order to get rid of annoying warnings.
	Also don't forget to remove .libs/lt-mytest and the directory
	.libs if possible.

2002-10-09  Murray Cumming  <murrayc@usa.net>

	* Changed version to 1.2.0, changing the library name too.

2002-09-10  Murray Cumming  <murrayc@usa.net>

	* Added the new manual, in DocBook XML format, by
	Ainsley Pereira <libsigc@pebble.org.uk>

2002-08-08  ERDI Gergo  <cactus@cactus.rulez.org>

	* sigc++/macros/slot.h.m4: Changed local Proxy typedef's name
	(used in constructors) to Proxy_ to suppress a warning about
	shadowing the class's Proxy typedef

2002-07-29  Murray Cumming  <murrayc@usa.net>

	* sigc++/sigc++.h: Removed include of convert.h, because it
	doesn't exist anymore.

1.1.12:
	
2002-07-27  ERDI Gergo  <cactus@cactus.rulez.org>
 
 	* sigc++/macros/retype_return.h.m4: Added hide_return() alias to
 	retype_return<void>
	
002-07-27  Murray Cumming  <murrayc@usa.net>

	* Improved the reference documentation, simplifying the groups to 
	Signals, Slots, bind, hide, retype, and Object.
	* And some white space to the Object/Control_ code and added TODOs
	asking for comments on mysterious parts.

2002-07-27  Murray Cumming  <murrayc@usa.net>

	* Simplicifaction, phase 2: Removed convert(). It is used
	extensively by a couple of projects, but one of those is a
	libsigc++ extension library anyway.

2002-07-26  Murray Cumming  <murrayc@usa.net>

	* Simplification, phase 1: Removing code that is not often used,
	and not essential to basic signal/slot usage, to make libsigc++
	smaller and simpler. This should help people to understand the
	code and encourage participation. The discarded code can be
	revived in an extra library if people need it enough to do that work.
	- Removed SigC::Closure, SigC::chain() and
	SigC::auto_disconnect(). One person says that they use chain(), 
	but it's not difficult to reproduce in local code.

2002-07-24  Murray Cumming  <murrayc@usa.net>

	* Added some spaces to make the code more readable.

2002-07-17  Murray Cumming  <murrayc@usa.net>

	* doc: Added reference directory, with a Doxygen file. It seems
	best to only generate HTML for classes that have been purposefully
	documented, because there's so much internals stuff.
	* Cleaned up the code a little, by adding some spaces and newlines.

1.1.11:
	
2002-06-09  Murray Cumming  <murrayc@usa.net>
 
	* sigc++/macros/signal.h.m4: Changed SignalExec from a struct to 
	a class because that's how it's declared elsewhere. MSVC++7 
	complained about this.
	* Added MSVC directory, which contains an MS Visual V++ project 
	file. However, it doesn't build because of a compiler error, and
	we probably need to distribute the generated files for this 
	anyway.

2002-06-08  Daniel Elstner  <daniel.elstner@gmx.net>

	* sigc++/sigc++.h: Replace #include "foo" with #include <foo>.
	Also include the headers bind_return.h, hide.h, method_slot.h.

2002-06-08  Daniel Elstner  <daniel.elstner@gmx.net>

	* doc/win32: Replace the file with Cedric's win32 build notes.
	The new text doesn't mention MSVC, but the MSVC notes in the old
	text were grossly outdated anyway.

2002-06-08  Daniel Elstner  <daniel.elstner@gmx.net>

	* .libs: Remove entire directory from CVS -- it didn't seem
	to fullfill any useful purpose.

2002-05-08  Cedric Gustin   <cgustin@ibelgique.com>

	* configure.in: Added support for win32 DLL (tests on win32
	platform + call to AC_LIBTOOL_WIN32).

	* sigc++/Makefile.am: Added support for win32 DLL (-no-undefined
	and -DLIBSIGC_COMPILATION).

	* sigc++/config/sigcconfig.h.in: Renamed LIBSIGC_WIN32 macro to
	LIBSIGC_DLL and LIBSIGC_UNIX to LIBSIGC_GCC to better separate the
	platform and compiler options. Added support for mingw32. Slightly
	changed the LIBSIGC_API (currently unused) to support latest
	versions of libtool.

2002-05-30  Daniel Elstner  <daniel.elstner@gmx.net>

	* configure.in (AC_OUTPUT): Don't try to chmod sigc-config-1.2 --
	the file doesn't exist anymore.

2002-05-03  Daniel Elstner  <daniel.elstner@gmx.net>

	* sigc++/macros/auto_disconnect.h.m4, sigc++/macros/hide.h.m4,
	sigc++/macros/signal.h.m4: Put 'typename' in front of type names
	which are members of a template class.  This is required by the
	C++ standard, and fixes the enormous amount of "implicit 'typename'
	is deprecated" warnings issued by gcc-3.1.

2002-04-28  Murray Cumming  <murrayc@usa.net>

	* sigc++/marsh.h: In Marshal<R&> specialization, value() now
	returns *value, instead of value, fixing a compilation
	error. Found by hingwah@gnupilgrims.org.

1.1.10
------
	
2002-04-27  Murray Cumming  <murrayc@usa.net>

	* Added scripts/sun.m4 to the distribution.

2002-04-17  Murray Cumming  <murrayc@usa.net>

	* Removed sigc-config and sigc++.m4. People should use
	pkg-config. We don't need 2 ways to do the same thing.

1.1.9
-----
	
2002-03-28  Karl Nelson

	* Removed --no-undefined linker option, which breaks build on
	Solaris/Forte.
	* method_slot: Added ConstMethodSlot to avoid "can't cast away
	constness" erros on Solaris/Forte.
	* This Changelog entry was written by murrayc, because Karl
	didn't. Karl later added this:
2002-03     Karl Nelson <kenelson@ece.ucdavis.edu>
        * Removed -no-undefined to fix Sun compiler.  No idea why it
          was in there in the first place.  File predated libsigc++.

2002-03-12  Daniel Elstner  <daniel.elstner@gmx.net>

	* autogen.sh: Change behaviour so that it works like the usual
	GNOME CVS autogen.sh, i.e. it does some sanity checks now and
	runs ./configure --enable-maintainer-mode automatically.
	* configure.in: No longer overwrite CFLAGS/CXXFLAGS/CPPFLAGS
	in maintainer mode.

1.1.8
-----
	
2002-02-25  Murray Cumming  <murrayc@usa.net>

	* Increased max number of Slot args from 5 to 6, because Gtk::Widget
	needs it.

1.1.7
-----
sigc-config is now sigc-config-1.2, to allow for parallel installation with libsigc++ 1.0
	
1.1.6
-----
Added missing Makefile.am entries for method_slot.h.
Added closure.h.m4 to macros/Makefile.am.

1.1.5
-----
binary incompatible.
changes to naming convention to improve clarity.
combined notify and disconnect as proved too confusing.
fixed several segmentation faults.
added method_slot adaptors.  
added closure (untested).
tested on Sun compiler.
tested on HPUX compiler.

1.1.4
-----

2001-10-17  Daniel Elstner  <daniel.elstner@gmx.net>

  * */Makefile.am:
    Made make dist and make distcheck work.  Excluded generated
    sources from distribution.

  * sigc++/macros/object_slot.h.m4:
    Added another pair of overloaded slot() functions, this time for
    references to non-const pointers (*&).  We have now 2*3*6 = 36
    overloaded slot() functions alone in object_slot.h. :(

2001-10-14  Daniel Elstner  <daniel.elstner@gmx.net>

  * signal.cc, signal.h.m4:  Implemented Signal_::create_slot.  I'm not sure
    wether the behaviour is correct, see comment there.  Moved SignalN<>::emit
    implementation to private static emit_ function, which is also used
    directly as proxy for Signal_::create_slot.  Thus code duplication in
    SignalN<>::proxy (removed, didn't work) was avoided.

  * object_slot.h.m4, object_slot_test.cc:  Reverted Murray Cumming's changes
    to ObjectSlot_ and SigC::slot templates.  For backward compatibility:
    overloaded SigC::slot to accept pointers, too.

  * examples/*.cc:  Include sigc++.h instead of signal_system.h,
    and other minor tweaks to make the examples work.  Corrected
    sigc++/README about this point, too.

  * To prevent header name clashes:  Overruled automake DEFAULT_INCLUDES
    to not include -I$(builddir) and -I$(srcdir).  Also changed every
    #include "foo.h" to #include "sigc++/foo.h".

1.1.3
-----
Optimized signal execution path (x3 speedup)
Fixed signal clear bug.
Added contributed hide adaptor.
Improved slot formulations.
Made headers more consistant.
Added LGPL license disclaimers to files.

1.1.2
-----
Improved formulation.
Dropped slot create functions.
Changed slot proxies to use references.
Asa adaptor.
