# Microsoft Developer Studio Project File - Name="libsigcxx" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** NICHT BEARBEITEN **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=libsigcxx - Win32 Release
!MESSAGE Dies ist kein g�ltiges Makefile. Zum Erstellen dieses Projekts mit NMAKE
!MESSAGE verwenden Sie den Befehl "Makefile exportieren" und f�hren Sie den Befehl
!MESSAGE 
!MESSAGE NMAKE /f "libsigc++_msvc6.mak".
!MESSAGE 
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "libsigc++_msvc6.mak" CFG="libsigcxx - Win32 Release"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "libsigcxx - Win32 Release" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libsigcxx - Win32 Debug" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libsigcxx - Win32 size_test" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libsigcxx - Win32 slot_test" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libsigcxx - Win32 signal_test" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libsigcxx - Win32 retype_test" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libsigcxx - Win32 retype_return_test" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libsigcxx - Win32 object_test" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libsigcxx - Win32 object_slot_test" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libsigcxx - Win32 method_slot_test" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libsigcxx - Win32 hide_test" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libsigcxx - Win32 disappearing_observer_test" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libsigcxx - Win32 connection_test" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libsigcxx - Win32 class_slot_test" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libsigcxx - Win32 bulk_test" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libsigcxx - Win32 bind_test" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libsigcxx - Win32 bind_return_test" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "libsigcxx - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "LIBSIGCXX_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "." /I "../" /I "../sigc++/config/" /I "../sigc++/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "LIBSIGCXX_EXPORTS" /D "LIBSIGC_COMPILATION" /D "DLL_EXPORT" /YX /FD /TP /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386 /out:"Release/libsigc++1.2-vc6.dll"

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "LIBSIGCXX_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "." /I "../" /I "../sigc++/config/" /I "../sigc++/" /D "_DEBUG" /D "_SIGCXX_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "LIBSIGCXX_EXPORTS" /D "LIBSIGC_COMPILATION" /D "DLL_EXPORT" /YX /FD /TP /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /out:"Debug/libsigc++1.2-vc6d.dll" /pdbtype:sept

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 size_test"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "libsigcxx___Win32_size_test"
# PROP BASE Intermediate_Dir "libsigcxx___Win32_size_test"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DebugTest"
# PROP Intermediate_Dir "DebugTest"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /I "." /I "sigc++/config/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "LIBSIGCXX_EXPORTS" /D "LIBSIGC_COMPILATION" /D "DLL_EXPORT" /YX /FD /TP /c
# ADD CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "../" /I "../sigc++/config/" /I "../sigc++/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo /o"DebugTest/size_test.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386 /out:"Release/libsigc++1.2.dll"
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /debug /machine:I386 /out:"DebugTest/size_test.exe"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 slot_test"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "libsigcxx___Win32_slot_test"
# PROP BASE Intermediate_Dir "libsigcxx___Win32_slot_test"
# PROP BASE Ignore_Export_Lib 1
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DebugTest"
# PROP Intermediate_Dir "DebugTest"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "sigc++/config/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT BASE CPP /Fr
# ADD CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "../" /I "../sigc++/config/" /I "../sigc++/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo /o"DebugTest/size_test.bsc"
# ADD BSC32 /nologo /o"DebugTest/slot_test.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /debug /machine:I386 /out:"DebugTest/size_test.exe"
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /debug /machine:I386 /out:"DebugTest/slot_test.exe"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 signal_test"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "libsigcxx___Win32_signal_test"
# PROP BASE Intermediate_Dir "libsigcxx___Win32_signal_test"
# PROP BASE Ignore_Export_Lib 1
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DebugTest"
# PROP Intermediate_Dir "DebugTest"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "sigc++/config/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT BASE CPP /Fr
# ADD CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "../" /I "../sigc++/config/" /I "../sigc++/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo /o"DebugTest/size_test.bsc"
# ADD BSC32 /nologo /o"DebugTest/size_test.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /debug /machine:I386 /out:"DebugTest/size_test.exe"
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /debug /machine:I386 /out:"DebugTest/signal_test.exe"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_test"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "libsigcxx___Win32_retype_test"
# PROP BASE Intermediate_Dir "libsigcxx___Win32_retype_test"
# PROP BASE Ignore_Export_Lib 1
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DebugTest"
# PROP Intermediate_Dir "DebugTest"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "sigc++/config/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT BASE CPP /Fr
# ADD CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "../" /I "../sigc++/config/" /I "../sigc++/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo /o"DebugTest/size_test.bsc"
# ADD BSC32 /nologo /o"DebugTest/size_test.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /pdb:"DebugTest/signal_test.pdb" /debug /machine:I386 /out:"DebugTest/signal_test.exe"
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /debug /machine:I386 /out:"DebugTest/retype_test.exe"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_return_test"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "libsigcxx___Win32_retype_return_test"
# PROP BASE Intermediate_Dir "libsigcxx___Win32_retype_return_test"
# PROP BASE Ignore_Export_Lib 1
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DebugTest"
# PROP Intermediate_Dir "DebugTest"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "sigc++/config/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT BASE CPP /Fr
# ADD CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "../" /I "../sigc++/config/" /I "../sigc++/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo /o"DebugTest/size_test.bsc"
# ADD BSC32 /nologo /o"DebugTest/size_test.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /pdb:"DebugTest/signal_test.pdb" /debug /machine:I386 /out:"DebugTest/signal_test.exe"
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /debug /machine:I386 /out:"DebugTest/retype_return_test.exe"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_test"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "libsigcxx___Win32_object_test"
# PROP BASE Intermediate_Dir "libsigcxx___Win32_object_test"
# PROP BASE Ignore_Export_Lib 1
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DebugTest"
# PROP Intermediate_Dir "DebugTest"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "sigc++/config/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT BASE CPP /Fr
# ADD CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "../" /I "../sigc++/config/" /I "../sigc++/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo /o"DebugTest/size_test.bsc"
# ADD BSC32 /nologo /o"DebugTest/size_test.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /pdb:"DebugTest/signal_test.pdb" /debug /machine:I386 /out:"DebugTest/signal_test.exe"
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /debug /machine:I386 /out:"DebugTest/object_test.exe"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_slot_test"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "libsigcxx___Win32_object_slot_test"
# PROP BASE Intermediate_Dir "libsigcxx___Win32_object_slot_test"
# PROP BASE Ignore_Export_Lib 1
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DebugTest"
# PROP Intermediate_Dir "DebugTest"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "sigc++/config/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT BASE CPP /Fr
# ADD CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "../" /I "../sigc++/config/" /I "../sigc++/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo /o"DebugTest/size_test.bsc"
# ADD BSC32 /nologo /o"DebugTest/size_test.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /pdb:"DebugTest/retype_return_test.pdb" /debug /machine:I386 /out:"DebugTest/retype_return_test.exe"
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /debug /machine:I386 /out:"DebugTest/object_slot_test.exe"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 method_slot_test"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "libsigcxx___Win32_method_slot_test"
# PROP BASE Intermediate_Dir "libsigcxx___Win32_method_slot_test"
# PROP BASE Ignore_Export_Lib 1
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DebugTest"
# PROP Intermediate_Dir "DebugTest"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "sigc++/config/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT BASE CPP /Fr
# ADD CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "../" /I "../sigc++/config/" /I "../sigc++/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo /o"DebugTest/size_test.bsc"
# ADD BSC32 /nologo /o"DebugTest/size_test.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /pdb:"DebugTest/retype_return_test.pdb" /debug /machine:I386 /out:"DebugTest/retype_return_test.exe"
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /debug /machine:I386 /out:"DebugTest/method_slot_test.exe"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 hide_test"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "libsigcxx___Win32_hide_test"
# PROP BASE Intermediate_Dir "libsigcxx___Win32_hide_test"
# PROP BASE Ignore_Export_Lib 1
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DebugTest"
# PROP Intermediate_Dir "DebugTest"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "sigc++/config/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT BASE CPP /Fr
# ADD CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "../" /I "../sigc++/config/" /I "../sigc++/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo /o"DebugTest/size_test.bsc"
# ADD BSC32 /nologo /o"DebugTest/size_test.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /pdb:"DebugTest/retype_return_test.pdb" /debug /machine:I386 /out:"DebugTest/retype_return_test.exe"
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /debug /machine:I386 /out:"DebugTest/hide_test.exe"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 disappearing_observer_test"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "libsigcxx___Win32_disappearing_observer_test"
# PROP BASE Intermediate_Dir "libsigcxx___Win32_disappearing_observer_test"
# PROP BASE Ignore_Export_Lib 1
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DebugTest"
# PROP Intermediate_Dir "DebugTest"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "sigc++/config/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT BASE CPP /Fr
# ADD CPP /nologo /MD /W3 /GX /ZI /Od /I "." /I "../" /I "../sigc++/config/" /I "../sigc++/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo /o"DebugTest/size_test.bsc"
# ADD BSC32 /nologo /o"DebugTest/size_test.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /pdb:"DebugTest/retype_return_test.pdb" /debug /machine:I386 /out:"DebugTest/retype_return_test.exe"
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /debug /machine:I386 /out:"DebugTest/disappearing_observer_test.exe"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 connection_test"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "libsigcxx___Win32_connection_test"
# PROP BASE Intermediate_Dir "libsigcxx___Win32_connection_test"
# PROP BASE Ignore_Export_Lib 1
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DebugTest"
# PROP Intermediate_Dir "DebugTest"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "sigc++/config/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT BASE CPP /Fr
# ADD CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "../" /I "../sigc++/config/" /I "../sigc++/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo /o"DebugTest/size_test.bsc"
# ADD BSC32 /nologo /o"DebugTest/size_test.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /pdb:"DebugTest/retype_return_test.pdb" /debug /machine:I386 /out:"DebugTest/retype_return_test.exe"
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /debug /machine:I386 /out:"DebugTest/connection_test.exe"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 class_slot_test"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "libsigcxx___Win32_class_slot_test"
# PROP BASE Intermediate_Dir "libsigcxx___Win32_class_slot_test"
# PROP BASE Ignore_Export_Lib 1
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DebugTest"
# PROP Intermediate_Dir "DebugTest"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "sigc++/config/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT BASE CPP /Fr
# ADD CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "../" /I "../sigc++/config/" /I "../sigc++/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo /o"DebugTest/size_test.bsc"
# ADD BSC32 /nologo /o"DebugTest/size_test.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /pdb:"DebugTest/retype_return_test.pdb" /debug /machine:I386 /out:"DebugTest/retype_return_test.exe"
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /debug /machine:I386 /out:"DebugTest/class_slot_test.exe"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bulk_test"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "libsigcxx___Win32_bulk_test"
# PROP BASE Intermediate_Dir "libsigcxx___Win32_bulk_test"
# PROP BASE Ignore_Export_Lib 1
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DebugTest"
# PROP Intermediate_Dir "DebugTest"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "sigc++/config/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT BASE CPP /Fr
# ADD CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "../" /I "../sigc++/config/" /I "../sigc++/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo /o"DebugTest/size_test.bsc"
# ADD BSC32 /nologo /o"DebugTest/size_test.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /pdb:"DebugTest/retype_return_test.pdb" /debug /machine:I386 /out:"DebugTest/retype_return_test.exe"
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /debug /machine:I386 /out:"DebugTest/bulk_test.exe"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_test"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "libsigcxx___Win32_bind_test"
# PROP BASE Intermediate_Dir "libsigcxx___Win32_bind_test"
# PROP BASE Ignore_Export_Lib 1
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DebugTest"
# PROP Intermediate_Dir "DebugTest"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "sigc++/config/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT BASE CPP /Fr
# ADD CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "../" /I "../sigc++/config/" /I "../sigc++/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo /o"DebugTest/size_test.bsc"
# ADD BSC32 /nologo /o"DebugTest/size_test.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /pdb:"DebugTest/retype_return_test.pdb" /debug /machine:I386 /out:"DebugTest/retype_return_test.exe"
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /debug /machine:I386 /out:"DebugTest/bind_test.exe"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_return_test"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "libsigcxx___Win32_bind_return_test"
# PROP BASE Intermediate_Dir "libsigcxx___Win32_bind_return_test"
# PROP BASE Ignore_Export_Lib 1
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DebugTest"
# PROP Intermediate_Dir "DebugTest"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "." /I "sigc++/config/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT BASE CPP /Fr
# ADD CPP /nologo /MD /W3 /Gm /GR- /GX /ZI /Od /I "../" /I "../sigc++/config/" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "LIBSIGC_COMPILATION" /D "_USRDLL" /D "DLL_EXPORT" /YX /FD /TP /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo /o"DebugTest/size_test.bsc"
# ADD BSC32 /nologo /o"DebugTest/size_test.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /pdb:"DebugTest/retype_return_test.pdb" /debug /machine:I386 /out:"DebugTest/retype_return_test.exe"
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /incremental:yes /debug /machine:I386 /out:"DebugTest/bind_return_test.exe"
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "libsigcxx - Win32 Release"
# Name "libsigcxx - Win32 Debug"
# Name "libsigcxx - Win32 size_test"
# Name "libsigcxx - Win32 slot_test"
# Name "libsigcxx - Win32 signal_test"
# Name "libsigcxx - Win32 retype_test"
# Name "libsigcxx - Win32 retype_return_test"
# Name "libsigcxx - Win32 object_test"
# Name "libsigcxx - Win32 object_slot_test"
# Name "libsigcxx - Win32 method_slot_test"
# Name "libsigcxx - Win32 hide_test"
# Name "libsigcxx - Win32 disappearing_observer_test"
# Name "libsigcxx - Win32 connection_test"
# Name "libsigcxx - Win32 class_slot_test"
# Name "libsigcxx - Win32 bulk_test"
# Name "libsigcxx - Win32 bind_test"
# Name "libsigcxx - Win32 bind_return_test"
# Begin Group "sources"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat;cc"
# Begin Source File

SOURCE="..\sigc++\adaptor.cc"
# End Source File
# Begin Source File

SOURCE="..\sigc++\bind.cc"
# End Source File
# Begin Source File

SOURCE=..\tests\bind_return_test.cc

!IF  "$(CFG)" == "libsigcxx - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 size_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 slot_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 signal_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 method_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 hide_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 disappearing_observer_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 connection_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 class_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bulk_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_return_test"

# PROP BASE Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\tests\bind_test.cc

!IF  "$(CFG)" == "libsigcxx - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 size_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 slot_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 signal_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 method_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 hide_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 disappearing_observer_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 connection_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 class_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bulk_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_test"

# PROP BASE Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\tests\bulk_test.cc

!IF  "$(CFG)" == "libsigcxx - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 size_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 slot_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 signal_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 method_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 hide_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 disappearing_observer_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 connection_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 class_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bulk_test"

# PROP BASE Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE="..\sigc++\class_slot.cc"
# End Source File
# Begin Source File

SOURCE=..\tests\class_slot_test.cc

!IF  "$(CFG)" == "libsigcxx - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 size_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 slot_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 signal_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 method_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 hide_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 disappearing_observer_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 connection_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 class_slot_test"

# PROP BASE Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bulk_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE="..\sigc++\connection.cc"
# End Source File
# Begin Source File

SOURCE=..\tests\connection_test.cc

!IF  "$(CFG)" == "libsigcxx - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 size_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 slot_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 signal_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 method_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 hide_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 disappearing_observer_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 connection_test"

# PROP BASE Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 class_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bulk_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\tests\disappearing_observer_test.cc

!IF  "$(CFG)" == "libsigcxx - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 size_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 slot_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 signal_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 method_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 hide_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 disappearing_observer_test"

# PROP BASE Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 connection_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 class_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bulk_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\tests\hide_test.cc

!IF  "$(CFG)" == "libsigcxx - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 size_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 slot_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 signal_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 method_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 hide_test"

# PROP BASE Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 disappearing_observer_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 connection_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 class_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bulk_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE="..\sigc++\method_slot.cc"
# End Source File
# Begin Source File

SOURCE=..\tests\method_slot_test.cc

!IF  "$(CFG)" == "libsigcxx - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 size_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 slot_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 signal_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 method_slot_test"

# PROP BASE Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 hide_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 disappearing_observer_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 connection_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 class_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bulk_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE="..\sigc++\node.cc"
# End Source File
# Begin Source File

SOURCE="..\sigc++\object.cc"
# End Source File
# Begin Source File

SOURCE="..\sigc++\object_slot.cc"
# End Source File
# Begin Source File

SOURCE=..\tests\object_slot_test.cc

!IF  "$(CFG)" == "libsigcxx - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 size_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 slot_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 signal_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_slot_test"

# PROP BASE Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 method_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 hide_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 disappearing_observer_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 connection_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 class_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bulk_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\tests\object_test.cc

!IF  "$(CFG)" == "libsigcxx - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 size_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 slot_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 signal_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_test"

# PROP BASE Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 method_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 hide_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 disappearing_observer_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 connection_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 class_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bulk_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\tests\retype_return_test.cc

!IF  "$(CFG)" == "libsigcxx - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 size_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 slot_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 signal_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_return_test"

# PROP BASE Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_slot_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 method_slot_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 hide_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 disappearing_observer_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 connection_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 class_slot_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bulk_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_return_test"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\tests\retype_test.cc

!IF  "$(CFG)" == "libsigcxx - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 size_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 slot_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 signal_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_test"

# PROP BASE Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 method_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 hide_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 disappearing_observer_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 connection_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 class_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bulk_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE="..\sigc++\signal.cc"
# End Source File
# Begin Source File

SOURCE=..\tests\signal_test.cc

!IF  "$(CFG)" == "libsigcxx - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 size_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 slot_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 signal_test"

# PROP BASE Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_return_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 method_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 hide_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 disappearing_observer_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 connection_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 class_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bulk_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\tests\size_test.cc

!IF  "$(CFG)" == "libsigcxx - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 size_test"

# PROP BASE Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 slot_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 signal_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 method_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 hide_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 disappearing_observer_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 connection_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 class_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bulk_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE="..\sigc++\slot.cc"
# End Source File
# Begin Source File

SOURCE=..\tests\slot_test.cc

!IF  "$(CFG)" == "libsigcxx - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 size_test"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 slot_test"

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 signal_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 retype_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 object_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 method_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 hide_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 disappearing_observer_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 connection_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 class_slot_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bulk_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libsigcxx - Win32 bind_return_test"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# End Group
# Begin Group "macros"

# PROP Default_Filter ".m4"
# Begin Source File

SOURCE="..\sigc++\macros\bind.h.m4"
# End Source File
# Begin Source File

SOURCE="..\sigc++\macros\bind_return.h.m4"
# End Source File
# Begin Source File

SOURCE=..\tests\bulk_test.cc.m4
# End Source File
# Begin Source File

SOURCE="..\sigc++\macros\class_slot.h.m4"
# End Source File
# Begin Source File

SOURCE="..\sigc++\macros\hide.h.m4"
# End Source File
# Begin Source File

SOURCE="..\sigc++\macros\method_slot.h.m4"
# End Source File
# Begin Source File

SOURCE="..\sigc++\macros\object_slot.h.m4"
# End Source File
# Begin Source File

SOURCE="..\sigc++\macros\retype.h.m4"
# End Source File
# Begin Source File

SOURCE="..\sigc++\macros\retype_return.h.m4"
# End Source File
# Begin Source File

SOURCE="..\sigc++\macros\signal.h.m4"
# End Source File
# Begin Source File

SOURCE="..\sigc++\macros\slot.h.m4"
# End Source File
# Begin Source File

SOURCE="..\sigc++\macros\template.macros.m4"
# End Source File
# End Group
# Begin Group "header"

# PROP Default_Filter ""
# Begin Source File

SOURCE="..\sigc++\adaptor.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\bind.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\bind_return.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\class_slot.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\connection.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\hide.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\marshal.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\method_slot.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\node.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\object.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\object_slot.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\retype.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\retype_return.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\sigc++.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\config\sigcconfig.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\signal.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\slot.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\trait.h"
# End Source File
# End Group
# End Target
# End Project
