Microsoft Embedded Visual C++ 4.0
==================================

Notes:
-------

- the compiler is basically the same as the one that comes with MSVC6, so most 
workarounds and bugs are the same. Also, the same procedure as described there 
is required to create some files from the .m4 templates.

- the C++ standardlibrary that comes with the compiler stinks, even worse than
the one that came with VC6. I therefore only tested with STLport (the yet 
unreleased version 5), so your experience may vary. However, this only affects
the unittests, libsigC++ does not depend on any other lib but the core C++ 
language support.

- under MS Windows, the program entry point is always WinMain(). VC6 does a good
job of hiding this in case one wants to use the standard main() function, but 
EVC4 doesn't, so you have to add a wrapper yourself. See main.cpp.

- the IDE still doesn't know that '.cc' is a C++ file, so you have to enter this 
information in the registry, the path is 
 Software\Microsoft\CEStudio\4.0\evc\Build System\Components\Platforms\
There are subfolders for each supported architecture, and inside those is a 
folder called tools that contains the folder with the settings for the compiler
named 'CL<platform>', e.g. CLARM for ARM processors. Inside that folder, there
is a key called 'Input_Spec', which tells the IDE(i.e. the build-system, but not
the compiler!) which type of files that tool handles.

- the compiler also doesn't know what a '.cc' file is, the switch '/Tp' added to 
the commandline tells it to treat the input as C++ code. Note that these settings
and those for the IDE are independent from each other, but both are required.

- in order to compile code with libsigC++, you need to add the root of the 
sourcetree and the sigc++/config subdir to the include paths.





Dealing with unresolved external symbols
-----------------------------------------
There is a bug in the compiler that causes link errors when a class that is 
exported from a DLL has a templated constructor and the compiler doesn't inline 
it because inlining is off. With VC6, you could switch to release mode, but 
with eVC4 I haven't found a way to make this work yet. For the unittests, this 
couses link errors for ObjectSlotNode, MethodSlotNode, ConstMethodSlotNode and 
ClassSlotNode.
A workaround is to provide explicit instantiations of the missing methods, 
which takes place in two steps: first, you need to create code that would call 
the ctor, then you can provide a version thereof.

So, if you have this error:

	unresolved external symbol ...
	SigC::ClassSlotNode::ClassSlotNode( 
		void (__cdecl*)(void *),
		class Foo *,
		void (__thiscall SigC::*)(void)
	)

then you create code that uses the ctor:

	void helper()
	{
		void (*proxy)(void*) = 0;
		Foo* object = 0;
		void (Foo::*method)() = 0;

		SigC::ClassSlotNode node( proxy, object, method);
	}

and finally the implementation thereof, copied almost verbatim from 
the original header:

	SigC::ClassSlotNode::ClassSlotNode(FuncPtr proxy,Foo* obj, void (Foo::*method)())
		: SlotNode(proxy), object_(obj), method_(reinterpret_cast<Method&>(method))
		{}

To be portable, you wrap all this in #if defined(UNDER_CE), or stuff it in a 
file that is only compiled under that compiler.


Steps taken for creating the projectfiles:
-------------------------------------------
I just list these up here for the curious (and for me, so I can look them up in 
case I need to do something similar). BTW: the projectfiles for the unittests
are all the same, they were created using sed (and afterwards made usable again 
with unix2dos - the IDE depends on the proper lineendings) to save typing.

For the library:

1. create empty DLL Project
2. add $(root) and $(root)/sigc++/config to the includes paths
3. add /Tp to the compiler commandline
4. add the macros LIBSIGC_COMPILATION and DLL_EXPORT to the defines
5. set libname uniformly to libsigc++-1.2-evc4[d].dll 

For the tests or other projects:
1. create an empty project
2. add $(root) and $(root)/sigc++/config to the includes paths
3. add /Tp to the compiler commandline
4. Either:
 - add a dependency on libsig++ in the same workspace, so the IDE automatically
   links in the right libs for it.
or
 - manually add the libs to the linker input


-- Ulrich Eckhardt


