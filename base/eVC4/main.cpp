#include <windows.h>
#include <sigc++/class_slot.h>

int main();


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
    OutputDebugString(_T("starting test...\n"));
    int res = main();
    OutputDebugString(_T("finished test.\n"));
    return res;
}
