# Microsoft Developer Studio Project File - Name="sigc" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=sigc - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "sigc.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "sigc.mak" CFG="sigc - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "sigc - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "sigc - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=xicl6.exe
RSC=rc.exe

!IF  "$(CFG)" == "sigc - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GR /GX /O2 /I "..\sigc++\config" /I "..\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FD /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=xilink6.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"Release\sigc-1.2.lib"

!ELSEIF  "$(CFG)" == "sigc - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GR /GX /ZI /Od /I "..\sigc++\config" /I "..\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FD /GZ /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=xilink6.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"Debug\sigc-1.2.debug.lib"

!ENDIF 

# Begin Target

# Name "sigc - Win32 Release"
# Name "sigc - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE="..\sigc++\adaptor.cc"
# End Source File
# Begin Source File

SOURCE="..\sigc++\bind.cc"
# End Source File
# Begin Source File

SOURCE="..\sigc++\class_slot.cc"
# End Source File
# Begin Source File

SOURCE="..\sigc++\connection.cc"
# End Source File
# Begin Source File

SOURCE="..\sigc++\method_slot.cc"
# End Source File
# Begin Source File

SOURCE="..\sigc++\node.cc"
# End Source File
# Begin Source File

SOURCE="..\sigc++\object.cc"
# End Source File
# Begin Source File

SOURCE="..\sigc++\object_slot.cc"
# End Source File
# Begin Source File

SOURCE="..\sigc++\signal.cc"
# End Source File
# Begin Source File

SOURCE="..\sigc++\slot.cc"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE="..\sigc++\adaptor.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\auto_disconnect.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\bind.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\bind_return.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\chain.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\class_slot.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\closure.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\connection.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\convert.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\hide.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\marshal.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\method_slot.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\node.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\object.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\object_slot.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\retype.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\retype_return.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\sigc++.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\signal.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\slot.h"
# End Source File
# Begin Source File

SOURCE="..\sigc++\trait.h"
# End Source File
# End Group
# End Target
# End Project
