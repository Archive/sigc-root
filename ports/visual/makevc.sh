#!/bin/sh

if test ! -f config.status ; then
  echo "Must be run in source directory"
  exit
fi

version=`grep SIGC_VERSION config.status | awk -F% '{print $3}'`
package="libsigc++-$version"
tarfile="$package.tar.gz"
project_file="$package/win32/libsigc.dsp"

echo "Unpack distribution."
if test -d $package; then
rm -R $package
fi
tar xzvf $tarfile

ccfiles=`find $package -name "*.cc"` 

echo "Renaming .cc files."
for i in $ccfiles ; 
do 
base=`echo $i | sed 's/\.cc$/.cpp/'`
echo "$i"
mv -f $i $base
done

echo "Altering project file."
mv $project_file $project_file.tmp
sed "s/\.cc/.cpp/" $project_file.tmp > $project_file
rm $project_file.tmp

echo "Removing irrelevent files."
find $package -name "Makefile*" | xargs rm
(cd $package; \
cp sigc++/config/sigc++config.h .
rm -Rf sigc++/config scripts config* acconfig.h sigc-config.in \
       aclocal.m4 ltdll.c autogen.sh libsigc++.spec borland djgpp riscos )

echo "Packing files."
find $package | zip -@ libsigc++-visual-$version.zip
rm -Rf $package

chmod a+r libsigc++-visual-$version.zip

