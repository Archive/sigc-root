/* sigc++/config/sigcconfig.h.  Generated automatically by configure.  */
/* sigc++/config/sigcconfig.h.in.  Generated automatically from configure.in by autoheader.  */
/*
  This file controls all the configurablity of sigc++ with regards
  to different compilers.  If you are begining a new port of sigc++
  to a compiler this is where to start.  

  Unix compilers are handled automatically by configure.  Other
  platforms require proper identification here.  To add a new
  port, first identify your compilers unique predefine and 
  create a LIBSIGC_{compiler} in the detection stage.  Then
  place a section which defines for the behavior of your compiler
  in the platform section.
*/
#ifndef _SIGC_CONFIG_H_
#define _SIGC_CONFIG_H_

// autoconf likes to place a lot of stuff we don't want.
#if 0

/* Name of package */
#define PACKAGE "libsigc++"

/* Version number of package */
#define VERSION "1.1.4"

#endif /* 0 */

#define SIGC_MAJOR_VERSION 1
#define SIGC_MINOR_VERSION 1
#define SIGC_MICRO_VERSION 4

// Detect common platforms

#if defined(__MWERKS__)
#define LIBSIGC_MWERKS
#elif defined(_WIN32)
// Win32 compilers have a lot of varation
#if defined(__BORLANDC__)
#define LIBSIGC_BC
#define LIBSIGC_WIN32
#elif defined(_MSC_VER)
#define LIBSIGC_MSC
#define LIBSIGC_WIN32
#elif defined(__CYGWIN__)
// cygwin is considered unix and doesn't need DLL decl.
#define LIBSIGC_UNIX
#else
#error "Unknown architecture (send me gcc --dumpspecs)"
#endif
#else
#define LIBSIGC_UNIX
#endif /* _WIN32 */


// Platform specific definitions

#ifdef LIBSIGC_UNIX
#define SIGC_CXX_INT_CTOR 1
#define SIGC_CXX_NAMESPACES 1
#define SIGC_CXX_PARTIAL_SPEC 1
#define SIGC_CXX_SPECIALIZE_REFERENCES 1
#define SIGC_CXX_VOID_RETURN 1
#endif /* LIBSIGC_UNIX */

#ifdef LIBSIGC_BC
#define SIGC_CXX_INT_CTOR 1
#define SIGC_CXX_NAMESPACES 1
#define SIGC_CXX_PARTIAL_SPEC 1
#define SIGC_CXX_SPECIALIZE_REFERENCES 1
#define SIGC_CXX_VOID_RETURN 1
//#define SIGC_CXX_MEMBER_FUNC_TEMPLATES 1
//#define SIGC_CXX_MEMBER_CLASS_TEMPLATES 1
//#define SIGC_CXX_MUTABLE 1
//#define SIGC_CXX_FRIEND_TEMPLATES 1
#endif /* LIBSIGC_BC */

#ifdef LIBSIGC_MSC
#define SIGC_CXX_INT_CTOR 1
#define SIGC_CXX_NAMESPACES 1
//#define SIGC_CXX_MEMBER_FUNC_TEMPLATES 1
//#define SIGC_CXX_MEMBER_CLASS_TEMPLATES 1
//#define SIGC_CXX_TEMPLATE_CCTOR 1
//#define SIGC_CXX_MUTABLE 1
#endif /* LIBSIGC_MSC */

#ifdef LIBSIGC_MWERKS
#define SIGC_CXX_INT_CTOR 1
#define SIGC_CXX_NAMESPACES 1
#define SIGC_CXX_PARTIAL_SPEC 1
#define SIGC_CXX_SPECIALIZE_REFERENCES 1
#define SIGC_CXX_VOID_RETURN 1
//#define SIGC_CXX_MEMBER_FUNC_TEMPLATES 1
//#define SIGC_CXX_MEMBER_CLASS_TEMPLATES 1
//#define SIGC_CXX_MUTABLE 1
#endif /* LIBSIGC_MWERKS */


// Window DLL declarations 

#ifdef LIBSIGC_WIN32
#ifdef LIBSIGC_EXPORTS
#define LIBSIGC_API __declspec(dllexport)
#define LIBSIGC_TMPL 
#else
#define LIBSIGC_API __declspec(dllimport)
#define LIBSIGC_TMPL extern 
#endif /* LIBSIGC_EXPORTS */
#else
#define LIBSIGC_API
#endif /* LIBSIGC_WIN32 */


#endif /* _SIGC_CONFIG_H_ */
