// Copyright 2001 Karl Einar Nelson
#include <iostream>
#include <sigc++/bind_return.h>

#ifdef SIGC_CXX_NAMESPACES
using namespace std;
using namespace SigC;
#endif

void foo() { }
int main()
  {
    int result;
    cout << ">>test 1"<<endl;
    Slot0<int> s=bind_return(slot(&foo),42);
    cout << (result=s()) << endl;
    return !(result==42);
  }
