// Copyright 2001 Karl Einar Nelson
#include <iostream>
#include <sigc++/bind.h>

#ifdef SIGC_CXX_NAMESPACES
using namespace std;
using namespace SigC;
#endif

int result=1;
struct A
  {
    A() { cout << "A()"<<endl;}
    A(int) { cout << "A(int)" <<endl;}
    A(const A&) { cout << "A(const A&)"<<endl;}
    ~A() { cout << "~A()"<<endl;}
  };

void foo(int i) { cout << "foo("<< i << ")" <<endl; result+=5;}
void foo2(A) { cout << "foo2(A)" <<endl; result+=3;}
int main()
  {
    cout << ">>test 1"<<endl;
    Slot0<void> s=bind(slot(&foo),1);
    s();
    cout << ">>test 2"<<endl;
    s=bind(slot(&foo2),1);
    s();
    cout << ">>test 3"<<endl;
    s.clear();
    cout << ">>end"<<endl;
    return !(result==9);
  }
