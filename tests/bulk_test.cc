#include <iostream>
#include <sigc++/slot.h>

#ifdef SIGC_CXX_NAMESPACES
using namespace std;
using namespace SigC;
#endif


int func_int_int_int(int a, int b) { return int(a*b); }

float func_float_float_float(float a, float b) { return float(a*b); }

double func_double_double_double(double a, double b) { return double(a*b); }

float func_float_int_int(int a, int b) { return float(a*b); }

int func_int_float_int(float a, int b) { return int(a*b); }

int func_int_int_float(int a, float b) { return int(a*b); }

int func_int_float_float(float a, float b) { return int(a*b); }

float func_float_int_float(int a, float b) { return float(a*b); }

float func_float_float_int(float a, int b) { return float(a*b); }

double func_double_int_int(int a, int b) { return double(a*b); }

int func_int_double_int(double a, int b) { return int(a*b); }

int func_int_int_double(int a, double b) { return int(a*b); }

int func_int_double_double(double a, double b) { return int(a*b); }

double func_double_int_double(int a, double b) { return double(a*b); }

double func_double_double_int(double a, int b) { return double(a*b); }

double func_double_float_float(float a, float b) { return double(a*b); }

float func_float_double_float(double a, float b) { return float(a*b); }

float func_float_float_double(float a, double b) { return float(a*b); }

float func_float_double_double(double a, double b) { return float(a*b); }

double func_double_float_double(float a, double b) { return double(a*b); }

double func_double_double_float(double a, float b) { return double(a*b); }


int main()
{
    Slot2<int,int,int> cb_int_int_int;
    cb_int_int_int = &func_int_int_int;
    cb_int_int_int(1,1);

    Slot2<float,float,float> cb_float_float_float;
    cb_float_float_float = &func_float_float_float;
    cb_float_float_float(1,1);

    Slot2<double,double,double> cb_double_double_double;
    cb_double_double_double = &func_double_double_double;
    cb_double_double_double(1,1);

    Slot2<float,int,int> cb_float_int_int;
    cb_float_int_int = &func_float_int_int;
    cb_float_int_int(1,1);

    Slot2<int,float,int> cb_int_float_int;
    cb_int_float_int = &func_int_float_int;
    cb_int_float_int(1,1);

    Slot2<int,int,float> cb_int_int_float;
    cb_int_int_float = &func_int_int_float;
    cb_int_int_float(1,1);

    Slot2<int,float,float> cb_int_float_float;
    cb_int_float_float = &func_int_float_float;
    cb_int_float_float(1,1);

    Slot2<float,int,float> cb_float_int_float;
    cb_float_int_float = &func_float_int_float;
    cb_float_int_float(1,1);

    Slot2<float,float,int> cb_float_float_int;
    cb_float_float_int = &func_float_float_int;
    cb_float_float_int(1,1);

    Slot2<double,int,int> cb_double_int_int;
    cb_double_int_int = &func_double_int_int;
    cb_double_int_int(1,1);

    Slot2<int,double,int> cb_int_double_int;
    cb_int_double_int = &func_int_double_int;
    cb_int_double_int(1,1);

    Slot2<int,int,double> cb_int_int_double;
    cb_int_int_double = &func_int_int_double;
    cb_int_int_double(1,1);

    Slot2<int,double,double> cb_int_double_double;
    cb_int_double_double = &func_int_double_double;
    cb_int_double_double(1,1);

    Slot2<double,int,double> cb_double_int_double;
    cb_double_int_double = &func_double_int_double;
    cb_double_int_double(1,1);

    Slot2<double,double,int> cb_double_double_int;
    cb_double_double_int = &func_double_double_int;
    cb_double_double_int(1,1);

    Slot2<double,float,float> cb_double_float_float;
    cb_double_float_float = &func_double_float_float;
    cb_double_float_float(1,1);

    Slot2<float,double,float> cb_float_double_float;
    cb_float_double_float = &func_float_double_float;
    cb_float_double_float(1,1);

    Slot2<float,float,double> cb_float_float_double;
    cb_float_float_double = &func_float_float_double;
    cb_float_float_double(1,1);

    Slot2<float,double,double> cb_float_double_double;
    cb_float_double_double = &func_float_double_double;
    cb_float_double_double(1,1);

    Slot2<double,float,double> cb_double_float_double;
    cb_double_float_double = &func_double_float_double;
    cb_double_float_double(1,1);

    Slot2<double,double,float> cb_double_double_float;
    cb_double_double_float = &func_double_double_float;
    cb_double_double_float(1,1);

	return 0;
}
