#include "slot.h"

define(`FUNC',`dnl
$1 func_$1_$2_$3($2 a, $3 b) { return $1(a*b); }
')


FUNC(int,int,int)
FUNC(float,float,float)
FUNC(double,double,double)
FUNC(float,int,int)
FUNC(int,float,int)
FUNC(int,int,float)
FUNC(int,float,float)
FUNC(float,int,float)
FUNC(float,float,int)
FUNC(double,int,int)
FUNC(int,double,int)
FUNC(int,int,double)
FUNC(int,double,double)
FUNC(double,int,double)
FUNC(double,double,int)
FUNC(double,float,float)
FUNC(float,double,float)
FUNC(float,float,double)
FUNC(float,double,double)
FUNC(double,float,double)
FUNC(double,double,float)
define(`CALL',`dnl
    Slot2<$1,$2,$3> cb_$1_$2_$3;
    cb_$1_$2_$3 = &func_$1_$2_$3;
    cb_$1_$2_$3(1,1);
')
int main()
{
CALL(int,int,int)
CALL(float,float,float)
CALL(double,double,double)
CALL(float,int,int)
CALL(int,float,int)
CALL(int,int,float)
CALL(int,float,float)
CALL(float,int,float)
CALL(float,float,int)
CALL(double,int,int)
CALL(int,double,int)
CALL(int,int,double)
CALL(int,double,double)
CALL(double,int,double)
CALL(double,double,int)
CALL(double,float,float)
CALL(float,double,float)
CALL(float,float,double)
CALL(float,double,double)
CALL(double,float,double)
CALL(double,double,float)
    return 0;
}
