// Copyright 2001 Karl Einar Nelson
#include <iostream>
#include <sigc++/class_slot.h>

#ifdef SIGC_CXX_NAMESPACES
using namespace std;
using namespace SigC;
#endif

int result=1;

class Foo 
  {
    int i;
    public:
    void foo()
      {
        cout << "hello "<<i <<endl; result+=3;
      }
    void foo2()
      {
        cout << "there "<<i <<endl; result+=5;
      }
    Foo() : i(1) {}
   };


int main()
  {
    Foo f;
    Slot0<void> s;
    cout << ">> call nothing connected" <<endl;
    s();

    cout << ">> call with foo" <<endl;
    s=slot_class(f,&Foo::foo);
    s();

    cout << ">> call with foo2" <<endl;
    s=slot_class(f,&Foo::foo2);
    s();

    cout << ">> call after clear" <<endl;
    s.clear();
    s();

    return !(result==9);
  }

#if defined(UNDER_CE) || (defined(_MSC_VER)&&defined(_DEBUG))
// see eVC4/README.txt
void helper()
{
    void (*proxy)(void*) = 0;
    Foo* object = 0;
    void (Foo::*method)() = 0;

    SigC::ClassSlotNode node( proxy, object, method);
}

SigC::ClassSlotNode::ClassSlotNode(FuncPtr proxy,Foo* obj, void (Foo::*method)())
    : SlotNode(proxy), object_(obj), method_(reinterpret_cast<Method&>(method))
    {}

#endif
