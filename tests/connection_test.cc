// Copyright 2001 Karl Einar Nelson
#include <iostream>
#include <sigc++/connection.h>

#ifdef SIGC_CXX_NAMESPACES
using namespace std;
using namespace SigC;
#endif

class TestConnectionNode: public ConnectionNode
  {
    public:
     TestConnectionNode(SlotNode* s): ConnectionNode(s) {}
     ~TestConnectionNode() { cout << "~ConnectionNode" <<endl; }
  };

void foo() 
  {}

int main()
  {
    bool correct;

    // create internals which we can control   
    SlotNode* s=new FuncSlotNode((FuncPtr)&foo,(FuncPtr)&foo);
    ConnectionNode *node=new TestConnectionNode(s);

    Connection c1,c2,c3;
    c1=Connection(node);
    c2=Connection(node);
    c3=Connection(node);
    correct=c1&&c2&&c3;
    cout << bool(c1) << bool(c2) << bool(c3) <<endl;
    c2.clear();
    correct|=c1&&!c2&&c3;
    cout << bool(c1) << bool(c2) << bool(c3) <<endl;
    c2=Connection(node);
    c1.disconnect();
    correct|=!c1&&!c2&&!c3;
    cout << bool(c1) << bool(c2) << bool(c3) <<endl;
    return !correct;
  }

