#include <sigc++/sigc++.h>

/// This class emits a signal in its destructor
struct foo
{
        ~foo() { signal.emit(); }
        SigC::Signal0<void> signal;
};

/// This class deletes itself when it receives a signal
struct bar :
        public SigC::Object
{
        bar(foo& Foo)
        {
               Foo.signal.connect(SigC::slot(*this, &bar::closed));
        }

        void closed()
        {
		delete this;
        }
};

int main(int, char**)
{
	foo f;
        new bar(f);

	return 0;
}



#if defined(UNDER_CE) || (defined(_MSC_VER)&&defined(_DEBUG))
// see eVC4/README.txt
void helper()
{
    void (*proxy)(void*) = 0;
    bar* control = 0;
    void* object = 0;
    void (bar::*method)() = 0;

    SigC::ObjectSlotNode node( proxy, control, object, method);
}

SigC::ObjectSlotNode::ObjectSlotNode(FuncPtr proxy,bar* control, void* object, void (bar::*method)())
      : SlotNode(proxy)
      { init(control,object,reinterpret_cast<Method&>(method)); }

#endif

