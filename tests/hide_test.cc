// Copyright 2001 Karl Einar Nelson
#include <iostream>
#include <sigc++/hide.h>

#ifdef SIGC_CXX_NAMESPACES
using namespace std;
using namespace SigC;
#endif

int result=1;

void foo() { cout << "foo()" <<endl; result+=3;}
void foo2(int) { cout << "foo2()" <<endl; result+=5;}
int main()
  {
    cout << ">>test 1"<<endl;
    Slot2<void,int,float> s=hide<int,float>(slot(&foo));
    s(1,1);
    cout << ">>test 2"<<endl;
    s=hide<float>(slot(&foo2));
    s(1,1);
    cout << ">>end"<<endl;
    return !(result==9);
  }
