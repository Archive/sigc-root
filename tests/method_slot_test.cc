// Copyright 2001 Karl Einar Nelson
#include <iostream>
#include <sigc++/method_slot.h>

#ifdef SIGC_CXX_NAMESPACES
using namespace std;
using namespace SigC;
#endif

int result=1;

class Foo 
  {
    int i;
    public:
    void foo()
      {
        cout << "hello "<<i <<endl; result+=3;
      }
    void foo2()
      {
        cout << "there "<<i <<endl; result+=5;
      }
    void foo_const() const
      {
        cout << "hello "<<i <<endl; result+=7;
      }
    void foo2_const() const
      {
        cout << "there "<<i <<endl; result+=11;
      }
    Foo() : i(1) {}

    ~Foo() 
      { cout << "~Foo()"<<endl; }
   };


int main()
  {
    cout << ">> test 1 (assignment to members)" << endl;
    {
      Foo f;

      Slot1<void,Foo&> s;
      cout << " call nothing connected" <<endl;
      s(f);

      cout << " call with foo" <<endl;
      s = slot(&Foo::foo);
      s(f);

      cout << " call with foo2" <<endl;
      s = slot(&Foo::foo2);
      s(f);

      cout << " call after clear" <<endl;
      s.clear();
    }

    cout << ">> test 2 (assignment to members const)" << endl;
    {
      Foo f;

      Slot1<void,const Foo&> s;
      cout << " call nothing connected" <<endl;
      s(f);

      cout << " call with foo" <<endl;
      s = slot(&Foo::foo_const);
      s(f);

      cout << " call with foo2" <<endl;
      s = slot(&Foo::foo2_const);
      s(f);

      cout << " call after clear" <<endl;
      s.clear();
      s(f);
    }

    cout<< result <<endl;

    return !(result==27);
  }


#if defined(UNDER_CE) || (defined(_MSC_VER)&&defined(_DEBUG))
// see eVC4/README.txt
void helper()
{
    void (*proxy)(void*) = 0;
    void (Foo::*method)() = 0;
    void (Foo::*const_method)() const = 0;

    SigC::MethodSlotNode node( proxy, method);
    SigC::ConstMethodSlotNode const_node( proxy, const_method);
}

SigC::MethodSlotNode::MethodSlotNode(FuncPtr proxy, void (Foo::*method)())
      : SlotNode(proxy)
      { init(reinterpret_cast<Method&>(method)); }

SigC::ConstMethodSlotNode::ConstMethodSlotNode(FuncPtr proxy, void (Foo::*method)() const)
      : SlotNode(proxy)
      { init(reinterpret_cast<Method&>(method)); }

#endif

