// Copyright 2001 Karl Einar Nelson
#include <iostream>
#include <sigc++/object.h>

#ifdef SIGC_CXX_NAMESPACES
using namespace std;
using namespace SigC;
#endif

class Foo : public Object
  {
    public:
    static bool alive;
    Foo()
      { cout << "Foo()" <<endl; alive=true;}
    ~Foo()
      { cout << "~Foo()" <<endl; alive=false;}
  };

bool Foo::alive;

int main()
  {
    bool fail=false;
    cout << ">> test 1 (containment)" <<endl;
    {
      Ptr<Foo> f=manage(new Foo());
      fail|=!Foo::alive;
    }
    fail|=Foo::alive;

    cout << "\n>> test 2 (extend)" <<endl;
    {
      Ptr<Foo> f2;
        {
          Ptr<Foo> f=manage(new Foo());
          f2=f;
          fail|=!Foo::alive;
        }
      cout << "(extend)" << endl;
      fail|=!Foo::alive;
    }
    fail|=Foo::alive;

    cout << "\n>> test 3 (destruction)" <<endl;
    {
      Ptr<Foo> f=manage(new Foo());
      cout << (Foo*)f << endl;
      delete f;
      fail|=Foo::alive;
      cout << (Foo*)f << endl;
    }
    fail|=Foo::alive;

    cout << "\n>> test 4 (self assign)" <<endl;
    {
      Ptr<Foo> f=manage(new Foo());
      f=f;
      fail|=!Foo::alive;
      cout << (Foo*)f << endl;
    }
    fail|=Foo::alive;

    // This is a compile-time test
    {
      Ptr<const Foo> f = manage(new Foo());
    }
    
    return fail;
  }

