// Copyright 2001 Karl Einar Nelson
#include <iostream>
#include <sigc++/retype_return.h>

#ifdef SIGC_CXX_NAMESPACES
using namespace std;
using namespace SigC;
#endif

int result=1;

float foo() { cout << "foo()" <<endl; result+=3; return 4.0; }
int main()
  {
    cout << ">>test 1"<<endl;
    Slot0<int> s=retype_return<int>(slot(&foo));
    cout << (result+=s()) <<endl;

    cout << ">>test 2"<<endl;
    Slot0<void> s2=retype_return<void>(slot(&foo));
    s2();
    
    return !(result==11);
  }
