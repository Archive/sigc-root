// Copyright 2001 Karl Einar Nelson
#include <iostream>
#include <sigc++/retype.h>

#ifdef SIGC_CXX_NAMESPACES
using namespace std;
using namespace SigC;
#endif

int result=1;

float foo(float i, float j)
  {
    result+=int (i*j);
    return i*j;
  }

int main()
  {
    cout << ">>test 1"<<endl;
#ifdef _MSC_VER
    Slot2<int,int,int> s=retype2<int,int,int>(slot(&foo));
#else
    Slot2<int,int,int> s=retype<int,int,int>(slot(&foo));
#endif
    cout << s(4,5)<<endl;

    cout << ">>test 2"<<endl;
#ifdef _MSC_VER
    Slot2<void,double,int> s2=retype2<void,double,int>(slot(&foo));
#else
    Slot2<void,double,int> s2=retype<void,double,int>(slot(&foo));
#endif
    s2(1,2);

    return !(result==23);
  }
