#include <iostream>
#include <sigc++/signal.h>
#include <sigc++/bind.h>

#ifdef SIGC_CXX_NAMESPACES
using namespace std;
using namespace SigC;
#endif

void foo(int i) { cout << "foo("<<i<<")" <<endl;}

int main()
  {
    // basic function
    {
      Signal0<void> sig;
      sig.connect(bind(slot(&foo),1));
      sig.connect(bind(slot(&foo),2));
      sig.connect(bind(slot(&foo),3));
      sig();
    }
    {
      Signal0<void> sig;
      Connection c=sig.connect(bind(slot(&foo),1));
      sig.connect(bind(slot(&foo),2));
      sig.connect(bind(slot(&foo),3));
      c.disconnect();
      sig();
    }
    {
      Signal0<void> sig;
      sig.connect(bind(slot(&foo),1));
      Connection c=sig.connect(bind(slot(&foo),2));
      sig.connect(bind(slot(&foo),3));
      c.disconnect();
      sig();
    }
    {
      Signal0<void> sig;
      sig.connect(bind(slot(&foo),1));
      sig.connect(bind(slot(&foo),2));
      Connection c=sig.connect(bind(slot(&foo),3));
      c.disconnect();
      sig();
    }

    return 0;
  }
