// Copyright 2001 Karl Einar Nelson
#include <iostream>
#include <sigc++/connection.h>
#include <sigc++/slot.h>
#include <sigc++/object.h>
#include <sigc++/object_slot.h>
#include <sigc++/class_slot.h>
#include <sigc++/signal.h>

#ifdef SIGC_CXX_NAMESPACES
using namespace std;
using namespace SigC;
#endif

int main()
  {
    cout << "Public classes:"   << endl
         << "  Connection "     << sizeof(Connection) << endl 
         << "  Object "         << sizeof(Object) << endl 
         << "  SlotBase "       << sizeof(SlotBase) << endl 
         << "  SignalBase "     << sizeof(SignalBase) << endl
         << "  ObjectBase "     << sizeof(ObjectBase) << endl
         << endl
         << "Internal classes:" << endl
         << "  Control_ "       << sizeof(Control_) << endl
         << "  BaseNode "       << sizeof(NodeBase) << endl
         << "  SlotNode "       << sizeof(SlotNode) << endl
         << "  FuncSlotNode "   << sizeof(FuncSlotNode) << endl
         << "  ConnectionNode " << sizeof(ConnectionNode) << endl
         << "  ObjectSlotNode " << sizeof(ObjectSlotNode) << endl
         << "  ClassSlotNode "  << sizeof(ClassSlotNode) << endl
         << "  SignalNode "     << sizeof(SignalNode) << endl
         << "  SignalConnectionNode " << sizeof(SignalConnectionNode) << endl; 
    return 0; // compile is passing
  }

