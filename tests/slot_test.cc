// Copyright 2001 Karl Einar Nelson
#include <iostream>
#include <sigc++/slot.h>

#ifdef SIGC_CXX_NAMESPACES
using namespace std;
using namespace SigC;
#endif

int result=1;

void foo()
  {
    cout << "hello" <<endl; result+=3;
  }
void foo2()
  {
    cout << "there" <<endl; result+=5;
  }


int main()
  {
    Slot0<void> s;
    cout << ">> call nothing connected" <<endl;
    s();

    cout << ">> call with foo" <<endl;
    s=&foo;
    s();

    cout << ">> call with foo2" <<endl;
    s=&foo2;
    s();

    cout << ">> call after clear" <<endl;
    s.clear();
    s();

    return !(result==9);
  }

